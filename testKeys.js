let testKeysObject = require('./keys');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const personDetailObject = { name: 'Satya', age: 23, location: 'Gorakhpur', degree: 'BCA', project: 1 };
const emptyObject = {};
const nullObject = null;
const undefinedObject = undefined;



testKeysObject.displayKeysArray(testKeysObject.keys(testObject));
testKeysObject.displayKeysArray(testKeysObject.keys(personDetailObject));
testKeysObject.displayKeysArray(testKeysObject.keys(emptyObject));
testKeysObject.displayKeysArray(testKeysObject.keys(nullObject));
testKeysObject.displayKeysArray(testKeysObject.keys(undefinedObject));



