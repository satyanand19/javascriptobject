let testPairObject = require('./pairs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const personDetailObject = { name: 'Satya', age: 23, location: 'Gorakhpur', degree: 'BCA', project: 1 };
const emptyObject = {};
const nullObject = null;
const undefinedObject = undefined;
const simpleObject = { a: 1, b: 2, c: 3, d: 4, e: 5 };


testPairObject.displayPairsArray(testPairObject.pairs(testObject));
testPairObject.displayPairsArray(testPairObject.pairs(personDetailObject));
testPairObject.displayPairsArray(testPairObject.pairs(emptyObject));
testPairObject.displayPairsArray(testPairObject.pairs(nullObject));
testPairObject.displayPairsArray(testPairObject.pairs(undefinedObject));
testPairObject.displayPairsArray(testPairObject.pairs(simpleObject));

