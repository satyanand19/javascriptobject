function values(obj) {

    if (!obj) {
        return [];
    }
    else {
        let valuesArray = [];
        for (let key in obj) {
            valuesArray.push(obj[key]);
        }
        return valuesArray;
    }
}

function displayValuesArray(valuesArray) {
    console.log(valuesArray);
}

module.exports = {
    values,
    displayValuesArray
}