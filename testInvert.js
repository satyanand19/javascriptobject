let testInvertObject = require('./invert');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testEmptyObject = {};
const testnull = null;
const testundefined = undefined;
const testPersonDetailsObject = { 'reg no': 11903555, age: 23, 'house no ': 95 };
const testSimpleObject = { 'BCA' : { 'Computer Science':'Competitive Coding'} };

testInvertObject.displayInvertObject(testInvertObject.invert(testObject));
testInvertObject.displayInvertObject(testInvertObject.invert(testEmptyObject));
testInvertObject.displayInvertObject(testInvertObject.invert(testnull));
testInvertObject.displayInvertObject(testInvertObject.invert(testundefined));
testInvertObject.displayInvertObject(testInvertObject.invert(testPersonDetailsObject));
testInvertObject.displayInvertObject(testInvertObject.invert(testSimpleObject));
