let testMapObject = require('./mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const personDetailObject = { name: 'Satya', age: 23, location: 'Gorakhpur', degree: 'BCA', project: 1 };
const emptyObject = {};
const nullObject = null;
const undefinedObject = undefined;



testMapObject.displayMapObject(testMapObject.mapObject(testObject, testMapObject.transformObject));
testMapObject.displayMapObject(testMapObject.mapObject(personDetailObject, testMapObject.transformObject));
testMapObject.displayMapObject(testMapObject.mapObject(emptyObject, testMapObject.transformObject));
testMapObject.displayMapObject(testMapObject.mapObject(nullObject, testMapObject.transformObject));
testMapObject.displayMapObject(testMapObject.mapObject(undefinedObject, testMapObject.transformObject));
