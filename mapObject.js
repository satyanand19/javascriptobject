function mapObject(obj, cb) {
    if (obj) {
        let mapObjectValues = {};
        for (let key in obj) {
            mapObjectValues[key] = cb(obj[key]);
        }
        return mapObjectValues;

    } else {
        return {};
    }
}

function transformObject(value) {
    return value * 2;
}

function displayMapObject(object) {
    console.log(object);
}

module.exports = {
    mapObject,
    transformObject,
    displayMapObject
};