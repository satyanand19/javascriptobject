function keys(obj) {

    if (!obj) {
        return [];
    }
    else {
        let keysArray = [];
        for (let key in obj) {
            keysArray.push(key);
        }
        return keysArray;
    }
}

function displayKeysArray(keysArray) {
    console.log(keysArray);
}

module.exports = {
    keys,
    displayKeysArray
}