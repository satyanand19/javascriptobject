let testDefaultsObject = require('./defaults');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const personDetailsObject = { name: 'Satyanand', age: 23, degree: 'BCA' };
const emptyObject = {};
const nullObject = null;
const undefinedObject = undefined;

testDefaultsObject.displayDefaultObject(testDefaultsObject.defaults(testObject, { degree: 'B.Tech CSE', age: 35 }));
testDefaultsObject.displayDefaultObject(testDefaultsObject.defaults(personDetailsObject, { location: 'Gorakhpur', age: 20 }));
testDefaultsObject.displayDefaultObject(testDefaultsObject.defaults(emptyObject, { degree: 'B.Tech CSE', age: 35 }));
testDefaultsObject.displayDefaultObject(testDefaultsObject.defaults(nullObject, { degree: 'B.Tech CSE', age: 35 })); //empty object
testDefaultsObject.displayDefaultObject(testDefaultsObject.defaults(undefinedObject, { degree: 'B.Tech CSE', age: 35 }));  //empty object

