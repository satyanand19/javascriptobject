let testValuesObject = require('./values');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const personDetailObject = { name: 'Satya', age: 23, location: 'Gorakhpur', degree: 'BCA', project: 1 };
const emptyObject = {};
const nullObject = null;
const undefinedObject = undefined;



testValuesObject.displayValuesArray(testValuesObject.values(testObject));
testValuesObject.displayValuesArray(testValuesObject.values(personDetailObject));
testValuesObject.displayValuesArray(testValuesObject.values(emptyObject));
testValuesObject.displayValuesArray(testValuesObject.values(nullObject));
testValuesObject.displayValuesArray(testValuesObject.values(undefinedObject));



