function defaults(obj, defaultProps) {
    if (obj) {
        let defaultsObject = obj;
        for (let key in defaultProps) {
            if (!obj[key]) {
                defaultsObject[key] = defaultProps[key];
            }
        }
        return defaultsObject;
    } else {
        return {};
    }
}

function displayDefaultObject(object) {
    console.log(object);
}

module.exports = {
    defaults,
    displayDefaultObject
};