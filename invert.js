function invert(obj) {
    if (obj) {
        let invertObject = {};
        for (let key in obj) {
            let string =JSON.stringify(obj[key]);
            invertObject[string] = key;
        }
        return invertObject;
    } else {
        return {};
    }
}

function displayInvertObject(invertObject) {
    console.log(invertObject);
}

module.exports = {
    invert,
    displayInvertObject
};