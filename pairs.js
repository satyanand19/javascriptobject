function pairs(obj) {
    if (obj) {
        let pairsArray = [];
        for (let key in obj) {
            pairsArray.push([key, obj[key]]);
        }
        return pairsArray;
    } else {
        return [];
    }
}

function displayPairsArray(pairsArray) {
    console.log(pairsArray);
}

module.exports = {
    pairs,
    displayPairsArray
};